" ln -s config/.vimrc .vimrc
"
"fix starting in replace mode on windoof
set t_u7= 
set ambw=double 

set nocompatible              " be iMproved, required
filetype off                  " required

" to set up vundle:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" YouCompleteMe
Plugin 'git://github.com/ycm-core/YouCompleteMe.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}
Plugin 'rdnetto/YCM-Generator'
" vimwiki
Plugin 'vimwiki'
" file browser
Plugin 'https://github.com/scrooloose/nerdtree.git'
Plugin 'https://github.com/bfrg/vim-cpp-modern.git'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" YCM settings
"https://wiki.archlinux.org/index.php/Vim/YouCompleteMe see this page for glob
let g:ycm_global_ycm_extra_conf = '~/config/.ycm_extra_conf.py'
let g:ycm_extra_conf_globlist = ['/mnt/c/Users/willmann/Desktop/SchindlerProject/*']
let g:ycm_extra_conf_globlist = ['/home/ludo/NewTec/*']
" write errors and warnings into the location list
let g:ycm_always_populate_location_list = 1

"NERDTree settings
"nnoremap <leader>n :NERDTreeFocus<CR>
"nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle .<CR>
"nnoremap <C-f> :NERDTreeFind<CR>

" clang-format settings
if has('python')
    map <C-K> :pyf /usr/share/clang/clang-format-11/clang-format.py<cr>
    "imap <C-K> <c-o>:pyf /usr/share/clang/clang-format-11/clang-format.py<cr>
elseif has('python3')
    map <C-K> :py3f /usr/share/clang/clang-format-11/clang-format.py<cr>
    "imap <C-K> <c-o>:py3f /usr/share/clang/clang-format-11/clang-format.py<cr>
endif

" Highlight Plugin settings
" Highlight struct/class member variables (affects both C and C++ files)
let g:cpp_member_highlight = 1

" Basic settings
syntax on
set number
set tabstop=4 softtabstop=0 expandtab
" set for a dark background
set background=dark
" set the coloscheme can be found in /usr/share/vim/vim82/colors/ Tab
" completion is also available ;)
" colo delek


" Keybindings See :help key-notation
map <F3> :YcmCompleter GoTo<Return>
"map <F4> :YcmCompleter GoToDeclaration<Return>

" vimgrep search the word under the cursor and open a quickfix list (:cw)
map <F4> :execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" vimwiki settings
let g:vimwiki_list = [{'path': '~/raid/vimwiki/',
                      \ 'path_html': '~/raid/vimwiki_html'}]

"" CHEATSHEET 
"of ycm config files
"
" J Join two lines
" C-o jump to older position
" C-i jump to newer position
